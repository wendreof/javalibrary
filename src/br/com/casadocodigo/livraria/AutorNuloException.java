package br.com.casadocodigo.livraria;

public class AutorNuloException extends RuntimeException {

    public AutorNuloException( String msg )
    {
        super(msg);
    }
}
