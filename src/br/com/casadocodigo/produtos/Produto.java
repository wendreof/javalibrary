package br.com.casadocodigo.produtos;

public interface Produto extends Comparable<Produto>{

    double getValor();
}
