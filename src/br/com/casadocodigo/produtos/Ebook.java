package br.com.casadocodigo.produtos;

import br.com.casadocodigo.livraria.Autor;

public class Ebook extends Livro implements Promocional {

    private String waterMark;

    public Ebook (Autor autor )
    {
        super( autor );
    }

    @Override
    public boolean aplicaDescontoDe( double porcentagem )
    {
        if( porcentagem > 0.15 )
        {
            return false;
        }
        else
        {
            System.out.println( "aplicando desconto no ebook" );
            double desconto =  this.getValor() * porcentagem;
            this.setValor( this.getValor() - desconto );
            return true;
        }
    }

    public String getWaterMark()
    {
        return waterMark;
    }

    public void setWaterMark( String waterMark )
    {
        this.waterMark = waterMark;
    }

    @Override
    public String toString()
    {
        return "Eu sou um Ebook";
    }
}
