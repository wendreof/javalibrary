package br.com.casadocodigo.produtos;

@FunctionalInterface
public interface Promocional {

    boolean aplicaDescontoDe( double porcentagem) ;
    //boolean naoSouMaisUmaInterfaceFuncional();

    default boolean aplicaDescontoDe10PorCento()
    {
        return aplicaDescontoDe( 0.1) ;
    }

}
